var app = angular.module('amazonApp', [
	'ngRoute',
	'amazonApp.controllers',
	'angular-loading-bar',
    'googleService',
    'securityService',
    'categoryService',
    'productService',
    'projectService',
]);
app.config(function($routeProvider,$locationProvider) {
    $locationProvider.hashPrefix('');
    $routeProvider
    .when("/product", {
        templateUrl : "app/components/product/index.html",
        controller: "productController",
        title:'product'
    })
    .when("/login", {
        templateUrl : "app/components/login/index.html",
        controller: "loginController",
        title:'login'
    })
    .when("/", {
        templateUrl : "app/components/login/index.html",
        controller: "loginController",
        title:'login'
    })
    .when("/g_data/:code", {
        templateUrl : "app/components/login/g_data.html",
        controller: "loginAuthController",
        title:'g_data'
    })
    .when("/detail/:product_id",{
        templateUrl : "app/components/detail/index.html",
        controller : "detailController",
        title : "Product Detail"
    })
    .when("/logout",{
        templateUrl : "app/components/logout/index.html",
        controller : "logoutController",
        title : "logout"
    })
    .when("/addBulk",{
        templateUrl : "app/components/product/bulk.html",
        controller : "productBulkController",
        title : "addbulk"
    })
    .when("/project/create",{
        templateUrl : "app/components/project/create.html",
        controller : "projectController",
        title : "project"
    })
    .when("/project/:project_id", {
        templateUrl : "app/components/project/view.html",
        controller: "projectViewController",
        title:'view project'
    })
    .otherwise({redirectTo:'/'});
});

//set constant
app.constant('base_url', 'http://c3mailer.com/angmazon/#/');
app.constant('base_url_pure', 'http://c3mailer.com/angmazon/');
app.constant('request_url', 'http://c3mailer.com/amazonApp/');

