app.controller('productBulkController', function(product,base_url,google,session,$scope,$location,$route,$routeParams, $window) {
	var title = $route.current.$$route.title;
    session.isLogin(title);
    $("#ng-side-nav").show();

    $scope.error_msg = false;

    $("#form-add-bulk-asin").click(function(){
		if($("#asin-bulk").val().replace(/\s/g,'') != "")
        {
        	var con = confirm('are you sure to import this ASIN?');

            if(con)
            {
                var asin = $('#asin-bulk').val().split('\n');
                // JSON.stringify(asin)
                product.requestAddBulk(JSON.stringify(asin)).then(function(result){
                	if(result.status)
                	{
                		$scope.error_msg = true;
                		$scope.error_class = 'alert-success';
                		$scope.error_text = result.msg;
                		$('#asin-bulk').val('');
                	}
                });

                
               
            }
        }
	});
 	
});