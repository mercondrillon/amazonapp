var productService = angular.module('productService', [])
.service('product', function (request_url,$http,$window) {
    var config = {
        headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    };
    this.requestAddBulk = function (asin) {
         var user_data = JSON.parse($window.sessionStorage.user_data);
        var data = $.param({
            asin: asin,
            id:user_data.user_data.id
        });
        var responseBulk = $http.post(request_url+'Ngrequest/addBulk',data,config).then(function (response) {
            return response.data;
        });

        return responseBulk;
    };
});
