var categoryService = angular.module('categoryService', [])
.service('category', function (request_url,$http) {
    this.getCategory = function () {
      var category = $http.get(request_url+'ngrequest/getCategory').then(function (response) {
          return response.data;
      });

      return category;
    };
});
