angular.module('amazonApp.controllers', []).
controller('loginController', function(base_url_pure,google,session,$scope,$location,$route) {
	$("#ng-side-nav").hide();
	$scope.navbar = base_url_pure+'app/shared/navbar.html';
 	var title = $route.current.$$route.title;
 	session.isLogin(title);
	$scope.title = title;
    google.getUrl().then(function(data){
		$scope.url =  data;
	});
});
app.controller('loginAuthController', function(base_url,google,session,$scope,$location,$route,$routeParams, $window) {
	$("#ng-side-nav").hide();
 	var title = $route.current.$$route.title;
 	var user_data;
	$scope.title = title;
	google.validateCode($routeParams.code).then(function(data){
		user_data = data;
	}).then(function(){
		session.setSession('user_data',JSON.stringify(user_data));
		$window.location.href = base_url+'product';
	});
 	
});